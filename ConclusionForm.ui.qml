import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls 1.4 as OC
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: item1
    width: 400
    height: 640

    property alias scrollView: scrollView

    Image {
        id: imageresult
        height: 87
        clip: true
        anchors.right: parent.right
        anchors.rightMargin: 16
        anchors.left: parent.left
        anchors.leftMargin: 17
        anchors.top: parent.top
        anchors.topMargin: 125
        Layout.maximumHeight: 120
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillHeight: true
        sourceSize.height: 0
        sourceSize.width: 0
        Layout.maximumWidth: 390
        fillMode: Image.Stretch
        Layout.fillWidth: true
        source: "Resources/Backgrounds/button_green.jpg"

        Label {
            id: labelconclusion
            color: "#f2f2f2"
            text: qsTr("Summary result: Good")
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            anchors.bottomMargin: 5
            anchors.topMargin: 5
            font.pointSize: 24
            fontSizeMode: Text.HorizontalFit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.fill: parent
        }
    }

    property alias listmodelconclusion: listModelconclusion

    ListView {
        id: listView
        visible: false
        clip: false
        cacheBuffer: 999
        spacing: 15
        keyNavigationWraps: false
        anchors.bottomMargin: 35
        anchors.rightMargin: 35
        anchors.leftMargin: 35
        anchors.topMargin: 180
        snapMode: ListView.NoSnap
        highlightRangeMode: ListView.ApplyRange

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        model: listModelconclusion
        delegate: Page {
            width: listView.width
            height: listView.height / 2
            Label {
                property string labeltext: date
                text: labeltext
                anchors.top: parent.top
                anchors.topMargin: 25
                font.pointSize: 16
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }

            TextArea {
                property string texttext: ""
                text: texttext
                font.capitalization: Font.Capitalize
                verticalAlignment: Text.AlignTop
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                renderType: Text.QtRendering
                font.pointSize: 14
                textFormat: Text.RichText
                placeholderText: "Tap here to write"
                anchors.right: parent.right
                anchors.rightMargin: 15
                anchors.left: parent.left
                anchors.leftMargin: 15
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 25
                anchors.top: parent.bottom
                anchors.topMargin: 45
                anchors.fill: parent
            }
        }
    }

    ScrollView {
        id: scrollView
        clip: true
        contentHeight: 1500
        contentWidth: -1
        anchors.rightMargin: 15
        anchors.leftMargin: 15
        anchors.bottomMargin: 15
        anchors.top: imageresult.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: 15

        Label {
            id: label_1
            text: "PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA "
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 0
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillWidth: true
        }

        Image {
            id: image_1
            height: 100
            fillMode: Image.PreserveAspectFit
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: label_1.bottom
            anchors.topMargin: 0
            Layout.fillHeight: true
            Layout.fillWidth: true
            source: "Resources/Backgrounds/line-chart-buninux-768x576.jpg"
        }

        Label {
            id: label_2
            text: "PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA "
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: image_1.bottom
            anchors.topMargin: 0
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        }

        Image {
            id: image_2
            height: 100
            fillMode: Image.PreserveAspectFit
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: label_2.bottom
            anchors.topMargin: 0
            source: "Resources/Backgrounds/word-cloud.jpg"
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Label {
            id: label_3
            text: qsTr("PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA PLEASE UPDATE DATA ")
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: image_2.bottom
            anchors.topMargin: 0
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        }
    }

    Image {
        id: imagedemofilter
        opacity: 0.9
        anchors.fill: parent
        source: "Resources/Backgrounds/gradient.jpg"

        Label {
            id: label
            width: 336
            height: 121
            color: "#ffffff"
            text: qsTr("Currently Unavailable")
            font.pointSize: 24
            fontSizeMode: Text.HorizontalFit
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    ListModel {
        id: listModelconclusion
        dynamicRoles: false
    }
}
