.pragma library

function letterlimit(checkval) {
var limval = 800; // limitation value calculated according to time of reading (800 letters per minute)
    var passed = false;
    if(checkval.length < limval) passed = true;
    return passed;
}
