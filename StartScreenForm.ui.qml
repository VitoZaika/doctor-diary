import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

Item {
    id: item1
    width: 540
    height: 1080
    property alias closesignin: closesignin
    property alias closesignup: closesignup
    property alias button: button
    property alias button1: button1
    property alias proceedtosignup: proceedtosignup
    property alias rawsigninup: rawsigninup
    property alias textFieldsignupname: textFieldsignupname
    property alias textFieldsigninpass: textFieldsigninpass
    property alias textFieldsigninlogin: textFieldsigninlogin
    property alias textFieldsignupage: textFieldsignupage
    property alias textFieldsignuppass: textFieldsignuppass
    property alias textFieldsignuplogin: textFieldsignuplogin
    property alias settingsbutton: settingsbutton
    property alias labelbalance: labelbalance
    property alias labelcurrentstate: labelcurrentstate
    property alias labelrecords: labelrecords
    property alias labelname: labelname
    property alias accountpage: accountpage
    property alias logoutbutton: logoutbutton
    property alias signbutton: signbutton

    Image {
        id: image
        anchors.fill: parent
        source: "Resources/Backgrounds/b_1.jpg"
    }

    ColumnLayout {
        id: columnLayout
        x: 25
        y: 85
        spacing: -90
        anchors.rightMargin: 25
        anchors.leftMargin: 25
        anchors.bottomMargin: 55
        anchors.topMargin: 85
        anchors.fill: parent

        FontLoader {
            id: webFont
            source: "qrc:/Resources/Fonts/Clarissa.ttf"
        }

        Label {
            id: label
            text: qsTr("Doctor Diary")
            font.family: webFont.name
            wrapMode: Text.WordWrap
            Layout.maximumHeight: 400
            Layout.maximumWidth: 210
            font.weight: Font.ExtraLight
            font.pointSize: 54
            fontSizeMode: Text.VerticalFit
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        Button {
            id: signbutton
            text: qsTr("Sign in with Google")
            Layout.fillWidth: false
            hoverEnabled: true
            display: AbstractButton.TextBesideIcon
            autoExclusive: false
            highlighted: true
            transformOrigin: Item.Center
            flat: false
            Layout.maximumHeight: 90
            Layout.maximumWidth: 320
            Layout.minimumHeight: 90
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.minimumWidth: 300

            Image {
                id: image1
                y: 23
                width: 59
                height: 60
                anchors.left: parent.left
                anchors.leftMargin: 15
                anchors.verticalCenter: parent.verticalCenter
                source: "Resources/Icons/Google.png"
            }
        }

        Button {
            id: rawsigninup
            text: qsTr("Sign up/Sign in")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.maximumWidth: 220
            Layout.maximumHeight: 80
            Layout.minimumHeight: 60
            Layout.minimumWidth: 200
        }

        Page {
            id: accountpage
            width: 200
            height: 200
            visible: false
            Layout.maximumHeight: 350
            Layout.maximumWidth: 350
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            contentHeight: 300
            contentWidth: 350
            title: "Account"

            Column {
                id: column
                spacing: 16
                anchors.fill: parent

                Label {
                    id: labelname
                    text: qsTr(
                              "Name: John Doe") // Has to be taken from google info OR from database
                    anchors.top: parent.top
                    anchors.topMargin: 15
                    anchors.right: parent.right
                    anchors.rightMargin: 15
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                    font.pointSize: 14
                    fontSizeMode: Text.HorizontalFit
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: labelrecords
                    text: qsTr("Records: 128")
                    anchors.top: labelname.bottom
                    anchors.topMargin: 15
                    anchors.left: parent.left
                    wrapMode: Text.WordWrap
                    font.pointSize: 14
                    anchors.leftMargin: 15
                    anchors.right: parent.right
                    fontSizeMode: Text.HorizontalFit
                    anchors.rightMargin: 15
                }

                Label {
                    id: labelcurrentstate
                    text: qsTr("Current state: Good")
                    anchors.left: parent.left
                    wrapMode: Text.WordWrap
                    anchors.leftMargin: 15
                    font.pointSize: 14
                    anchors.top: labelrecords.bottom
                    anchors.right: parent.right
                    anchors.topMargin: 15
                    anchors.rightMargin: 15
                    fontSizeMode: Text.HorizontalFit
                }

                Label {
                    id: labelbalance
                    text: qsTr("Balance: Balanced")
                    anchors.left: parent.left
                    wrapMode: Text.WordWrap
                    font.pointSize: 14
                    anchors.leftMargin: 15
                    anchors.top: labelcurrentstate.bottom
                    anchors.right: parent.right
                    anchors.topMargin: 15
                    fontSizeMode: Text.HorizontalFit
                    anchors.rightMargin: 15
                }

                Button {
                    id: settingsbutton
                    text: qsTr("Settings")
                    anchors.left: parent.left
                    visible: true
                    Layout.minimumWidth: 200
                    Layout.minimumHeight: 60
                    Layout.maximumHeight: 80
                    Layout.maximumWidth: 220
                    anchors.leftMargin: 15
                    anchors.top: labelbalance.bottom
                    anchors.right: parent.right
                    anchors.topMargin: 15
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                    anchors.rightMargin: 15
                }

                Button {
                    id: logoutbutton
                    text: qsTr("Log Out")
                    anchors.top: settingsbutton.bottom
                    anchors.topMargin: 15
                    anchors.right: parent.right
                    anchors.rightMargin: 15
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                    visible: true
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                    Layout.maximumWidth: 220
                    Layout.maximumHeight: 80
                    Layout.minimumWidth: 200
                    Layout.minimumHeight: 60
                }
            }
        }

        RowLayout {
            id: rowLayout
            width: 100
            height: 100
            visible: false
            Layout.fillWidth: false
            spacing: 26
        }
    }

    Page {
        id: pagesign
        x: 50
        y: 235
        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 25
        anchors.left: parent.left
        anchors.leftMargin: 25
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 150
        anchors.top: parent.top
        anchors.topMargin: 150

        ColumnLayout {
            id: columnLayoutsignup
            visible: false
            anchors.rightMargin: 15
            anchors.leftMargin: 15
            anchors.bottomMargin: 15
            anchors.topMargin: 15
            anchors.fill: parent

            Label {
                id: label1
                text: qsTr("Sign Up")
                font.pointSize: 24
                fontSizeMode: Text.Fit
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            }

            Label {
                id: label3
                text: qsTr("Sign Up")
                font.pointSize: 24
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignHCenter
                Layout.fillWidth: true
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: textFieldsignuplogin
                text: qsTr("")
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                font.weight: Font.Light
                font.capitalization: Font.MixedCase
                font.pointSize: 14
                Layout.fillWidth: true
                placeholderText: "Login (your email)"
            }

            Label {
                id: label4
                text: qsTr("Sign Up")
                font.pointSize: 24
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignHCenter
                Layout.fillWidth: true
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: textFieldsignupname
                text: qsTr("")
                font.pointSize: 14
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                font.weight: Font.Light
                placeholderText: "Login (your email)"
                Layout.fillWidth: true
                font.capitalization: Font.MixedCase
            }

            TextField {
                id: textFieldsignuppass
                echoMode: TextInput.PasswordEchoOnEdit
                text: qsTr("")
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                font.weight: Font.Light
                placeholderText: "Password"
                Layout.fillWidth: true
                font.pointSize: 14
                font.capitalization: Font.MixedCase
            }

            TextField {
                id: textFieldsignupage
                text: qsTr("")
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                font.weight: Font.Light
                placeholderText: "Age"
                Layout.fillWidth: true
                font.pointSize: 14
                echoMode: TextInput.PasswordEchoOnEdit
                font.capitalization: Font.MixedCase
            }

            Button {
                id: button
                text: qsTr("Continue")
                flat: false
                highlighted: true
                font.pointSize: 12
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }

            Button {
                id: closesignup
                text: qsTr("Continue")
                font.pointSize: 12
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                flat: false
                highlighted: true
            }
        }

        ColumnLayout {
            id: columnLayoutsignin
            x: -6
            y: 9
            visible: false
            anchors.fill: parent
            Label {
                id: label2
                text: qsTr("Sign In")
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 24
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                verticalAlignment: Text.AlignVCenter
                fontSizeMode: Text.Fit
            }

            TextField {
                id: textFieldsigninlogin
                text: qsTr("")
                font.weight: Font.Light
                placeholderText: "Login (your email)"
                Layout.fillWidth: true
                font.pointSize: 14
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                font.capitalization: Font.MixedCase
            }

            TextField {
                id: textFieldsigninpass
                text: qsTr("")
                font.weight: Font.Light
                placeholderText: "Password"
                Layout.fillWidth: true
                font.pointSize: 14
                echoMode: TextInput.PasswordEchoOnEdit
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                font.capitalization: Font.MixedCase
            }

            Button {
                id: button1
                text: qsTr("Continue")
                highlighted: true
                font.pointSize: 12
                flat: false
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }

            anchors.leftMargin: 15
            anchors.bottomMargin: 15
            anchors.topMargin: 15
            anchors.rightMargin: 15
            Label {
                id: label5
                text: qsTr("Sign In")
                font.pointSize: 24
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignHCenter
                Layout.fillWidth: true
                verticalAlignment: Text.AlignVCenter
            }

            Button {
                id: proceedtosignup
                text: qsTr("Continue")
                font.pointSize: 12
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                flat: false
                highlighted: true
            }

            Button {
                id: closesignin
                text: qsTr("Continue")
                font.pointSize: 12
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                flat: false
                highlighted: true
            }
        }
    }
    states: [
        State {
            name: "logged"

            PropertyChanges {
                target: signbutton
                visible: false
            }

            PropertyChanges {
                target: rawsigninup
                visible: false
            }

            PropertyChanges {
                target: logoutbutton
                text: qsTr("Log Out")
                visible: true
            }

            PropertyChanges {
                target: accountpage
                visible: true
            }

            PropertyChanges {
                target: label
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
                visible: true
            }
        },
        State {
            name: "signup"

            PropertyChanges {
                target: pagesign
                visible: true
            }

            PropertyChanges {
                target: columnLayoutsignup
                visible: true
            }

            PropertyChanges {
                target: textFieldsignupname
                placeholderText: "Name (or nickname)"
            }

            PropertyChanges {
                target: label3
                height: 33
                text: "Notice: to keep your identity anonimous, we will not store your e-mail in our database"
                Layout.columnSpan: 1
                Layout.rowSpan: 1
                textFormat: Text.AutoText
                horizontalAlignment: Text.AlignLeft
                fontSizeMode: Text.Fit
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
                font.pointSize: 8
                wrapMode: Text.WordWrap
            }

            PropertyChanges {
                target: label4
                text: qsTr("Your name or nickname to display in service")
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pointSize: 8
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            }

            PropertyChanges {
                target: closesignup
                text: qsTr("Close")
                flat: true
                highlighted: false
            }

            PropertyChanges {
                target: textFieldsignuplogin
                placeholderText: "E-mail"
            }

            PropertyChanges {
                target: textFieldsigninlogin
                placeholderText: "E-mail"
            }
        },
        State {
            name: "signin"

            PropertyChanges {
                target: pagesign
                visible: true
            }

            PropertyChanges {
                target: columnLayoutsignin
                visible: true
            }

            PropertyChanges {
                target: label5
                color: "#5f9fff"
                text: "If you are not registered yet, click on Sign up button below"
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
                font.pointSize: 16
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }

            PropertyChanges {
                target: proceedtosignup
                text: qsTr("Sign up")
            }

            PropertyChanges {
                target: closesignin
                text: qsTr("Close")
                flat: true
                highlighted: false
            }

            PropertyChanges {
                target: button1
                highlighted: false
            }

            PropertyChanges {
                target: textFieldsignuplogin
                placeholderText: "E-mail"
            }

            PropertyChanges {
                target: textFieldsigninlogin
                placeholderText: "E-mail"
            }
        }
    ]
}
