import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls 1.4 as OC
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4
import Qt.labs.calendar 1.0

Item {
    id: item1
    width: 540
    height: 1080

    property alias listmodeltt: listModeltt

    ListView {
        id: listView
        clip: false
        cacheBuffer: 999
        spacing: 15
        keyNavigationWraps: false
        anchors.bottomMargin: 150
        anchors.rightMargin: 35
        anchors.leftMargin: 35
        anchors.topMargin: 180
        snapMode: ListView.SnapToItem
        highlightRangeMode: ListView.ApplyRange

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        model: listModeltt
        delegate: RowLayout {
            id: rowLayout
            spacing: 12
            width: listView.width / 1.25
            height: listView.height / 1.5

            Label {
                property string labeltext: date
                text: labeltext
                Layout.fillWidth: true
                Layout.minimumHeight: 50
                Layout.minimumWidth: 80
                anchors.top: parent.top
                anchors.topMargin: 25
                font.pointSize: 16
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                //                anchors.horizontalCenter: parent.horizontalCenter
            }
            TextArea {
                property string texttext: ""
                text: texttext
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.fillWidth: true
                Layout.minimumHeight: 70
                Layout.minimumWidth: 240
                font.capitalization: Font.Capitalize
                verticalAlignment: Text.AlignTop
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                renderType: Text.QtRendering
                font.pointSize: 14
                textFormat: Text.RichText
                placeholderText: "Tap here to write (maximum amount of letters: 800)"

                Image {
                    opacity: 0.3
                    z: -1
                    source: "Resources/Backgrounds/whiteblank.png"

                    anchors.fill: parent
                }

                onEditingFinished: z = 0

                onPressed: z = 1

                //anchors.right: parent.right
                //anchors.rightMargin: 15
                //                anchors.left: parent.left
                //                anchors.leftMargin: 15
                //                anchors.bottom: parent.bottom
                //                anchors.bottomMargin: 25
                //                anchors.top: parent.bottom
                //                anchors.topMargin: 45
                //                anchors.fill: parent
            }
        }

        //            Page {
        //            width: listView.width
        //            height: listView.height / 2
        //            Label {
        //                property string labeltext: date
        //                text: labeltext
        //                anchors.top: parent.top
        //                anchors.topMargin: 25
        //                font.pointSize: 16
        //                verticalAlignment: Text.AlignVCenter
        //                horizontalAlignment: Text.AlignHCenter
        //                anchors.horizontalCenter: parent.horizontalCenter
        //            }
        //            TextArea {
        //                property string texttext: ""
        //                text: texttext
        //                font.capitalization: Font.Capitalize
        //                verticalAlignment: Text.AlignTop
        //                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        //                renderType: Text.QtRendering
        //                font.pointSize: 14
        //                textFormat: Text.RichText
        //                placeholderText: "Tap here to write"
        //                anchors.right: parent.right
        //                anchors.rightMargin: 15
        //                anchors.left: parent.left
        //                anchors.leftMargin: 15
        //                anchors.bottom: parent.bottom
        //                anchors.bottomMargin: 25
        //                anchors.top: parent.bottom
        //                anchors.topMargin: 45
        //                anchors.fill: parent
        //            }
        //        }
    }

    ListModel {
        id: listModeltt
        dynamicRoles: false
    }
}
