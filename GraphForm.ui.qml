import QtQuick 2.9
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.3
import QtQuick.Templates 2.2
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Item {
    id: item1
    width: 400
    height: 640

    ColumnLayout {
        id: columnLayout
        clip: true
        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 188
        anchors.top: parent.top
        anchors.topMargin: 125

        Image {
            id: image
            width: 100
            height: 100
            Layout.fillHeight: true
            Layout.maximumHeight: 450
            Layout.maximumWidth: 390
            fillMode: Image.PreserveAspectFit
            Layout.fillWidth: true
            source: "Resources/Backgrounds/line-chart-buninux-768x576.jpg"
        }
    }

    Page {
        id: page
        font.pointSize: 11
        clip: true
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 75
        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.top: columnLayout.bottom
        anchors.topMargin: 0
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillHeight: false
        Layout.fillWidth: true
        Layout.minimumHeight: 150
        Layout.minimumWidth: 250

        Label {
            id: label
            text: qsTr("Trend is of blablabla type. Positive mood prevails in last 12h. Condition can be considered as balanced")
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            anchors.bottomMargin: 5
            anchors.topMargin: 5
            anchors.fill: parent
            font.pointSize: 11
            fontSizeMode: Text.Fit
            wrapMode: Text.WordWrap
        }
    }
}
