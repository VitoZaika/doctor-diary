#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDataStream>
#include <QDebug>
#include <QFile>
#include "QtSql/QSqlDatabase"
#include "QSqlQuery"
#include <QQmlContext>
#include <QQmlEngine>
#include <QFontDatabase>
// use statusbar for making a better appearance through transparent style
#include "statusbar.h"

#include "googleapiwrapper.h"


int main(int argc, char *argv[])
{
    qputenv("QT_QUICK_CONTROLS_STYLE", "material");
    qputenv("QT_QUICK_CONTROLS_MATERIAL_THEME", "Light");
    qputenv("QT_QUICK_CONTROLS_MATERIAL_ACCENT", "Blue");

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    QFontDatabase qfd;
    qfd.addApplicationFont(":/Resources/Fonts/Clarissa.ttf");

    GoogleAPIWrapper googleapiwrapper;
    engine.rootContext()->setContextProperty("GoogleAPIWrapper", &googleapiwrapper);
    qmlRegisterType<GoogleAPIWrapper>("GAW", 1, 0, "GoogleAPIWrapper");

    qmlRegisterType<StatusBar>("StatusBar", 0, 1, "StatusBar");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
