#ifndef SQLITECUSTOM_H
#define SQLITECUSTOM_H

#include <QObject>

class sqlitecustom : public QObject
{
    Q_OBJECT

public:

    explicit sqlitecustom(QObject *parent = 0);

signals:

public slots:

    void addrecord(std::string record);
};

#endif // SQLITECUSTOM_H
