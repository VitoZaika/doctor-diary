import QtQuick 2.5
import "crypt.js" as Crypt
import "stringhelper.js" as SH

StartScreenForm {

    signbutton.onClicked: {

        GoogleAPIWrapper.grant();
    }

    rawsigninup.onClicked: { // Switch to sign up/sign in menu
        idstartscreen.state = "signin";
    }

    proceedtosignup.onClicked: { // Proceed to signing up
        idstartscreen.state = "signup";
    }

    button1.onClicked: { // Raw sign in action

    }

    closesignin.onClicked: {
        idstartscreen.state = "base state";
    }

    closesignup.onClicked: {
        idstartscreen.state = "base state";
    }
}
