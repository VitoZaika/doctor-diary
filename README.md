# Doctor Diary

Academic project based on sentiment analysis.
The idea of this project is the ability to know the emotional condition of person and giving the automatic reports and advices to user.
Project contains several important parts:
1) Server-side apps (ML, account management and logging actions)
2) Mobile client apps (iOS&Android)
3) Web app (sharing features with mobile client)