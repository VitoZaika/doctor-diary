import QtQuick 2.4

ConclusionForm {

    Connections{

        target: scrollView
        Component.onCompleted: {

            refreshHeight();

        }

    }

    // type - chart/image or text content; text - URL in first case or text itself in the last one
    function updateData(type, text){

    }

    function refreshHeight(){
        scrollView.contentHeight = scrollView.childrenRect.height + 25
    }
}
