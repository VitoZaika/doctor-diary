import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtQuick.Controls 1.4 as OC
import QtQuick.Controls.Styles 1.4
import "DatabaseT.js" as JS // import helper to use SQlite etc
import StatusBar 0.1 // use this to customize status bar color
import QtQuick.Controls.Material 2.2

ApplicationWindow {
    visible: true
    //visibility: Window.FullScreen
    title: qsTr("Scroll")
    flags: Qt.Window | Qt.MaximizeUsingFullscreenGeometryHint

    property alias headerval : label.text // variable for header
    property alias viewindex : view.currentIndex // assign current index to view index to check page transitions
    property alias subviewindex : subview.currentIndex // same but with subview

    //*USE CAREFULLY*******
    readonly property int dpi: Screen.pixelDensity * 25.4
        function dp(x){ return (dpi < 120) ? x : x*(dpi/160); }
        //******************

    onViewindexChanged: {
        switch(view.currentIndex){
        case 0:
            view.interactive = true
            subview.currentIndex = 0
            break;
        case 1:
            view.interactive = false
            break;
        }

    }

    onSubviewindexChanged: {
        switch(subview.currentIndex){
        case 0:
            headerval = "Diary Records"
            break;
        case 1:
            headerval = "Charts"
            break;
        case 2:
            headerval = "Wordcloud"
            break;
        case 3:
            headerval = "Conclusion"
            break;
        }
    }

    StatusBar {
        theme: StatusBar.Light
        color: Material.color(Material.Grey, Material.Shade100)
    }

    SwipeView {
            id: view
            interactive: false
            anchors.fill: parent

            Item {
                    id: firstPage
                    Rectangle{
                        anchors.fill: parent
                        color: 'transparent'
                        StartScreen{
                            id: idstartscreen
                            anchors.fill: parent
                        }
                    }
                }

            Item {

                id: secondPage

                Image {
                    id: image
                    anchors.fill: parent
                    source: "Resources/Backgrounds/b_1_1.jpg"
                }

                Page {
                    id: page
                    height: 120
                    z: 1
                    clip: true
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0

                    Button { //
                        id: calendarbutton
                        text: qsTr("")
                        visible: true
                        clip: false
                        font.pointSize: 14
                        highlighted: true
                        flat: true
                        anchors.fill: calendarid

                        Connections{
                            target: calendarbutton
                            onClicked:{
                                page.clip = false
                                imagefilter.visible = true
                                mouseAreafilter.enabled = true
                            }
                        }
                    }

                    Image {
                        id: image_hamburger
                        width: dp(24)
                        height: dp(24)
                        anchors.left: parent.left
                        anchors.leftMargin: dp(24)
                        fillMode: Image.PreserveAspectFit
                        anchors.verticalCenter: label.verticalCenter
                        source: "Resources/Icons/kisspng.png"

                        MouseArea {
                            id: mouseAreahamburger
                            enabled: true
                            anchors.fill: parent
                            onClicked:{
                                view.currentIndex = 0
                            }
                        }

                    }


                            OC.Calendar {
                                id: calendarid
                                height: 394
                                anchors.top: label.bottom
                                anchors.topMargin: 0
                                anchors.right: parent.right
                                anchors.rightMargin: 0
                                anchors.left: parent.left
                                anchors.leftMargin: 0
                                opacity: 1
                                z: 0
                                clip: false
                                visible: true
                                visibleYear: 2018
                                visibleMonth: 10
                                dayOfWeekFormat: 1
                                weekNumbersVisible: false
                                navigationBarVisible: true
                                frameVisible: true
                                style: CalendarStyle {
                                    gridVisible: false
                                    dayDelegate: Rectangle {
                                        gradient: Gradient {
                                            GradientStop {
                                                position: 0.00
                                                color: styleData.selected ? "#111" : (styleData.visibleMonth
                                                                                      && styleData.valid ? "#444" : "#666")
                                            }
                                            GradientStop {
                                                position: 1.00
                                                color: styleData.selected ? "#444" : (styleData.visibleMonth
                                                                                      && styleData.valid ? "#111" : "#666")
                                            }
                                            GradientStop {
                                                position: 1.00
                                                color: styleData.selected ? "#777" : (styleData.visibleMonth
                                                                                      && styleData.valid ? "#111" : "#666")
                                            }
                                        }

                                        Label {

                                            text: styleData.date.getDate()
                                            anchors.centerIn: parent
                                            color: styleData.valid ? "white" : "grey"
                                        }

                                        Rectangle {
                                            width: parent.width
                                            height: 1
                                            color: "#555"
                                            anchors.bottom: parent.bottom
                                        }

                                        Rectangle {
                                            width: 1
                                            height: parent.height
                                            color: "#555"
                                            anchors.right: parent.right
                                        }
                                    }
                                }

                                Connections{
                                    target: calendarid
                                    onPressed:{
                                        page.clip = true
                                        imagefilter.visible = false
                                        mouseAreafilter.enabled = false
                                    }
                                }
                            }
                    FontLoader {
                        id: webFont
                        source: "qrc:/Resources/Fonts/Clarissa.ttf"
                    }

                    Label {
                        id: label
                        x: 155
                        y: 5
                        text: headerval // alternative : qsTr("Diary Records")
                        anchors.top: parent.top
                        anchors.topMargin: 5
                        font.family: webFont.name
                        font.weight: Font.ExtraLight
                        wrapMode: Text.WordWrap
                        font.pointSize: 32
                        fontSizeMode: Text.HorizontalFit
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        anchors.right: parent.right
                        anchors.rightMargin: 115
                        anchors.left: parent.left
                        anchors.leftMargin: 115
                        anchors.verticalCenterOffset: -20
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }

                SwipeView {
                        id: subview
                        interactive: true
                        currentIndex: 0
                        anchors.fill: parent



                        Item {

                            id: subPageDiaryList
                            Rectangle{
                                anchors.fill: parent
                                color: 'transparent'
                                DiaryList{
                                    id: iddiarylist
                                    anchors.fill: parent
                                    }
                            }

                        }

                        Item {
                                id: subPageGraph
                                Rectangle{
                                    anchors.fill: parent
                                    color: 'transparent'
                                    Graph{
                                        id: idgraph
                                        anchors.fill: parent
                                    }
                                }
                            }

                        Item {
                                id: subPageWordcloud
                                Rectangle{
                                    anchors.fill: parent
                                    color: 'transparent'
                                    Wordcloud{
                                        id: idwordcloud
                                        anchors.fill: parent
                                    }
                                }
                            }

                        Item {
                                id: subPageConclusion
                                Rectangle{
                                    anchors.fill: parent
                                    color: 'transparent'
                                    Conclusion{
                                        id: idconclusion
                                        anchors.fill: parent
                                    }
                                }
                            }

                }

                    MouseArea {
                        id: mouseAreafilter
                        enabled: false
                        anchors.fill: parent
                        onClicked:{
                            page.clip = true
                            imagefilter.visible = false
                            mouseAreafilter.enabled = false
                        }
                    }

                    Image {
                        id: imagefilter
                        visible: false
                        opacity: 0.6
                        fillMode: Image.Stretch
                        anchors.fill: parent
                        source: "Resources/Backgrounds/gradient.jpg"
                    }
                }



    }

    PageIndicator {
        id: indicator

        count: view.count + subview.count - 1 // subtract one because swipe struct has another one swipe struct built in it's second page
        currentIndex: view.currentIndex + subview.currentIndex

        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }


    // using block below to connect c++ signals to QML part
    Connections{

        target: GoogleAPIWrapper
        onAuthenticated: {
            console.log("QML email = " + emailval + "QML etag = " + etagpass)
            idstartscreen.state = "logged";
            view.interactive = true;
        }

    }

    Component.onCompleted: headerval = "Diary Records" // Set this header value by default

}




