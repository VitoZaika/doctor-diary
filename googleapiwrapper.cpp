//Class for using Google API (currently just to verify user email and get the name)

#include "googleapiwrapper.h"
#include <QtNetworkAuth>
#include <QtGui>
#include <QtCore>
#include <QOAuth2AuthorizationCodeFlow>
#include <QOAuthHttpServerReplyHandler>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QAbstractOAuth>
#include <QString>


GoogleAPIWrapper::GoogleAPIWrapper(QObject *parent) : QObject(parent)
{

    //Open JSON file with data
    QFile file(":/client_id.json");
    file.open(QIODevice::ReadOnly | QIODevice::Text);


    QJsonParseError jsonError;
    QJsonDocument flowerJson = QJsonDocument::fromJson(file.readAll(),&jsonError);

    //Configure Google API part
    google.setScope("email");

    //Parse JSON file
    const auto object = flowerJson.object();
    const auto settingsObject = object["web"].toObject();
    const QUrl authUri(settingsObject["auth_uri"].toString());
    const auto clientId = settingsObject["client_id"].toString();
    const QUrl tokenUri(settingsObject["token_uri"].toString());
    const auto clientSecret(settingsObject["client_secret"].toString());
    const auto redirectUris = settingsObject["redirect_uris"].toArray();
    const QUrl redirectUri(redirectUris[0].toString()); // Get the first URI
    const auto port = static_cast<quint16>(redirectUri.port()); // Get the port


    //Configure API
    google.setAuthorizationUrl(authUri);
    google.setClientIdentifier(clientId);
    google.setAccessTokenUrl(tokenUri);
    google.setClientIdentifierSharedKey(clientSecret);

    //Adjust reply handler
    auto replyHandler = new QOAuthHttpServerReplyHandler(port, this);
    google.setReplyHandler(replyHandler);

    // using platform-standard browser for auth
    connect(&google, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser,
                &QDesktopServices::openUrl);

    // when authenticated, translate a signal to update
    connect(this, &GoogleAPIWrapper::authenticated, this, &GoogleAPIWrapper::update);

    // when permission granted getting the email and name from JSON response
    connect(&google, &QOAuth2AuthorizationCodeFlow::granted, [&](){ //=
            qDebug() << __FUNCTION__ << __LINE__ << "Access Granted!";
            auto reply = google.get(QUrl("https://www.googleapis.com/plus/v1/people/me"));
            connect(reply, &QNetworkReply::finished, [reply, this](){
                qDebug() << "REQUEST FINISHED. Error? " << (reply->error() != QNetworkReply::NoError);

                // parse the reply to JSON
                QByteArray resultarray = reply->readAll();
                QJsonParseError jsonresultError;
                QJsonDocument result = QJsonDocument::fromJson(resultarray,&jsonresultError);
                QJsonObject obj = result.object();

                // get the username
                const auto username = obj["displayName"].toString();

                // get the etag as non-permanent password (for users who use google login)
                const auto etagpass = obj["etag"].toString();

                // get the emails object
                QJsonValue emails = obj.value("emails");
                QJsonArray emailsObj = emails.toArray();

                // first registered email is the google's one, so we taking it
                QJsonValue firsteval = emailsObj[0];
                QJsonObject eobj = firsteval.toObject();
                const auto emailval = eobj["value"].toString();

                // checking the output
                //qDebug() << "Email: " << emailval << " Name: " << username << "Etag: " << etagpass;
                //qDebug() << "Response: " << result; // use this to get complete response
                emit authenticated(emailval, username, etagpass);

            });

        });

}

void GoogleAPIWrapper::grant()
{

    google.grant();

}

void GoogleAPIWrapper::update(){


}
