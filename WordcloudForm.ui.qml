import QtQuick 2.9
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.3
import QtQuick.Templates 2.2
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4 as OC
import QtQuick.Controls.Styles 1.4

Item {
    id: item1
    width: 400
    height: 640

    ColumnLayout {
        id: columnLayout
        clip: true
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 150
        anchors.right: parent.right
        anchors.rightMargin: 16
        anchors.left: parent.left
        anchors.leftMargin: 17
        anchors.top: page1.bottom
        anchors.topMargin: 0

        Image {
            id: image
            width: 100
            height: 100
            Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
            Layout.fillHeight: true
            sourceSize.height: 1024
            sourceSize.width: 1024
            Layout.maximumWidth: 390
            fillMode: Image.PreserveAspectFit
            Layout.fillWidth: true
            source: "Resources/Backgrounds/word-cloud.jpg"
        }
    }

    Page {
        id: page
        height: 87
        clip: true
        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.top: columnLayout.bottom
        anchors.topMargin: 0
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillHeight: false
        Layout.fillWidth: true
        Layout.minimumHeight: 150
        Layout.minimumWidth: 250

        Label {
            id: label
            text: qsTr("Amount of unique words: 378")
            anchors.fill: parent
            font.pointSize: 9
            fontSizeMode: Text.HorizontalFit
            wrapMode: Text.WordWrap
        }
    }

    Page {
        id: page1
        x: -7
        height: 91
        clip: true
        anchors.top: parent.top
        anchors.topMargin: 125
        Label {
            id: label1
            text: "5 most common: refresh,refresh,refresh,refresh,refresh"
            anchors.topMargin: 0
            anchors.fill: parent
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            font.pointSize: 12
            fontSizeMode: Text.Fit
        }
        anchors.left: parent.left
        Layout.minimumHeight: 150
        Layout.minimumWidth: 250
        Layout.fillWidth: true
        anchors.leftMargin: 15
        Layout.fillHeight: false
        anchors.right: parent.right
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        anchors.rightMargin: 15
    }
}
