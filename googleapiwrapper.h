#ifndef GOOGLEAPIWRAPPER_H
#define GOOGLEAPIWRAPPER_H
#include <QtCore>
#include <QtNetwork>

#include <QOAuth2AuthorizationCodeFlow>

class GoogleAPIWrapper : public QObject
{

    Q_OBJECT

public:

    explicit GoogleAPIWrapper(QObject *parent = 0);

public slots:
    void grant();
    void update();
signals:
    void authenticated(QString emailval, QString username, QString etagpass);

private:
    QOAuth2AuthorizationCodeFlow google;
};

#endif // GOOGLEAPIWRAPPER_H
